
// eslint-disable-next-line no-unused-vars
class Terrain {
    constructor () {
        // selectionner le terrain et annuler les cliques si on est pas entrain de jouer
        this.terrain = document.getElementsByClassName('terrain')[0]
        this.terrain.addEventListener('click', function (event) {
            // eslint-disable-next-line no-undef
            if (!Terrain.isRunning) {
                event.preventDefault()
                event.stopPropagation()
            }
        }, true)

        // nb de cartes dans le terrain
        this.NB_CARTES = 16
        Terrain.nbPaires = this.NB_CARTES / 2
        // le tableau qui va contenir les positions des cartes
        this.positionCartes = [this.NB_CARTES]
        // les position disponibles a prendre pour les cartes
        this.placesDispo = [this.NB_CARTES]
        for (let i = 0; i < this.NB_CARTES; i++) {
            this.placesDispo[i] = i
        }

        // positionner chaque carte
        for (let i = 1; i <= this.NB_CARTES / 2; i++) {
            // generer deux positions aleatoire pour chaque carte
            this.positionCartes[this.genererPosition()] = i
            this.positionCartes[this.genererPosition()] = i
        }

        // initialiser le timer
        Terrain.minute = 0
        Terrain.seconde = 0
        Terrain.milliSeconde = 0

        // initialiser le menu
        Terrain.initMenu()
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour remplir le terrain avec les cartes
    genererTerrain () {
        this.terrain.innerHTML = ''

        for (let i = 0; i < this.positionCartes.length; i++) {
            // creation d'un container de chaque carte
            const divContainer = document.createElement('DIV')
            divContainer.classList.add('flip-container')
            divContainer.classList.add('carte-' + this.positionCartes[i])
            divContainer.onclick = function () { Terrain.clickCard(divContainer) }

            // 2eme container pour le flipp
            const divFlipper = document.createElement('DIV')
            divFlipper.classList.add('flipper')

            // la face de la carte
            const divFront = document.createElement('DIV')
            divFront.classList.add('front')
            divFront.innerHTML = '<img src="images/carte-back.png" alt="back card" />'

            // le dos de la carte
            const divBack = document.createElement('DIV')
            divBack.classList.add('back')
            divBack.innerHTML = '<img src="images/img-' + this.positionCartes[i] + '.png" alt="img-' + this.positionCartes[i] + '" />'

            // mettre la face et le dos dans le container du flipp
            divFlipper.appendChild(divFront)
            divFlipper.appendChild(divBack)

            // mettre le flipp dans le grands container de la carte
            divContainer.appendChild(divFlipper)

            // mettre la carte dans le terrain
            this.terrain.appendChild(divContainer)
        }
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour generer une position aleatoire
    genererPosition () {
        const caseAleatoire = Math.floor(Math.random() * this.placesDispo.length) // prendre une case aleatoirement des places dispos

        const position = this.placesDispo[caseAleatoire] // trouver la position dans le tableau des places dispos selon la case genere

        this.placesDispo.splice(caseAleatoire, 1) // enlever la position generer pour ne pas la regener une autre fois

        return position
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour afficher les cartes du terrain pour un laps de temps
    afficherCartesPourUnLapsDeTemps () {
        const listeCartes = document.getElementsByClassName('flip-container')
        for (let i = 0; i < listeCartes.length; i++) {
            // pour chaque carte on l'affiche un laps de temps
            window.setTimeout(function () {
                if (Terrain.clickDeuxFois) {
                    listeCartes[i].classList.add('flipped')
                }

                // apres on la cache
                window.setTimeout(function () {
                    listeCartes[i].classList.remove('flipped')
                }, 1500)
            }, 700)
        }
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour gerer le clique sur une carte
    static clickCard (divContainer) {
        const cardsFlipped = document.getElementsByClassName('flipped')
        // 1---- si on a deja clique sur 2 carte donc on les reflipp
        if (cardsFlipped.length === 2) {
            window.clearTimeout(Terrain.flippCardTime)
            Terrain.reflipp()
        }

        // 2---- on flipp la carte
        divContainer.classList.toggle('flipped')

        // 3---- si c la deuxieme cliquee et les 2 cartes sont identiques donc on les laisse flippees
        if (cardsFlipped.length === 2 && cardsFlipped[0].classList[1] === cardsFlipped[1].classList[1]) {
            // on les laisse flippees
            cardsFlipped[0].classList.add('identique')
            cardsFlipped[1].classList.add('identique')

            // incrementer le nombre de paires trouvees
            Terrain.pairesTrouvees += 1
            document.getElementById('paires').value = Terrain.pairesTrouvees

            // si la partie est finis
            if (Terrain.pairesTrouvees === Terrain.nbPaires) {
                // lancer le son de joie
                document.getElementById('son-fin-jeu').play()

                // on arrete la partie
                document.getElementById('button-partie').innerText = 'Nouvelle Partie'

                Terrain.isRunning = false
                window.clearInterval(Terrain.tempsPartie)

                // on affiche la fenetre de fin de partie
                Terrain.siRecordBattu()
            }

            // on enleve la class flipped
            Terrain.reflipp()

            // si les deux cartes sont pas identiques donc on les cache
        } else if (cardsFlipped.length === 2) {
            Terrain.flippCardTime = window.setTimeout(Terrain.reflipp, 1000)
        }
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour annuler les flipp apres un laps de temps
    static reflipp () {
        document.getElementsByClassName('flipped')[0].classList.remove('flipped')
        document.getElementsByClassName('flipped')[0].classList.remove('flipped')
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour verifier si le record est battu
    static siRecordBattu () {
        // on calcule le nombde de ms
        const tempsFinPartie = (Terrain.minute * 60 * 1000) + (Terrain.seconde * 1000) + (Terrain.milliSeconde * 100)

        // afficher la fenetre de felicitation
        document.getElementsByClassName('temps-fin')[0].innerText = Terrain.minute + 'min ' + Terrain.seconde + 's ' + Terrain.milliSeconde + 'ms'
        document.getElementsByClassName('felicitation')[0].classList.add('afficher-felicitation')
        const finPartie = document.getElementsByClassName('jeu')[0]
        finPartie.classList.add('fin-partie')

        // faire disparaitre le felicitation apres un clique sur la fenetre
        finPartie.addEventListener('click', Terrain.cacherFelecitation, true)

        // afficher un message de bien joue
        const messageFinPartie = document.getElementById('message-bien-joue')

        if (Terrain.meilleurScore === 0 || tempsFinPartie < Terrain.meilleurScore) {
            // enregistrer le temps record
            Terrain.meilleurScore = tempsFinPartie
            document.getElementById('score').value = Terrain.minute + ' . ' + Terrain.seconde + ' . ' + Terrain.milliSeconde

            // Terrain.setRecord(Terrain.meilleurScore) <-------------- on peut l'utiliser

            messageFinPartie.innerText = 'Tres Bien joue, vous avez battu le record'
        } else if (tempsFinPartie > Terrain.meilleurScore) {
            messageFinPartie.innerText = 'Bien joue, '
        }
    }

    // fonction static pour enlever la fenetre felicitation
    static cacherFelecitation () {
        // calculer un laps de temps pour cache la fenetre
        window.setTimeout(function () {
            document.getElementsByClassName('felicitation')[0].classList.remove('afficher-felicitation')
            document.getElementsByClassName('jeu')[0].classList.remove('fin-partie')

            // enlever le listener apres le click
            document.getElementsByClassName('jeu')[0].removeEventListener('click', Terrain.cacherFelecitation, true)
        }, 600)
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour lancer le timer de la partie
    static lancerTimer () {
        Terrain.milliSeconde += 1
        if (Terrain.milliSeconde === 10) {
            Terrain.milliSeconde = 0
            Terrain.seconde += 1
            if (Terrain.seconde % 60 === 0) {
                Terrain.seconde = 0
                Terrain.minute += 1
            }
        }
        document.getElementById('temps').value = Terrain.minute + ' . ' + Terrain.seconde + ' . ' + Terrain.milliSeconde
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    // fonction pour reinitialiser le menu
    static initMenu () {
        // reinitialiser le nombre de paires
        Terrain.pairesTrouvees = 0
        document.getElementById('paires').value = Terrain.pairesTrouvees

        // on reinitialise le temps de partie affiche
        document.getElementById('temps').value = '0 . 0 . 0'
    }

    // -----------------------------------------------------------------------------------------------------------------------------------

    /** cette fonction est inutilisable
     * car on doit heberger en local la page pour que ca marche
     */

    // fonctions pour enregistrer le record
    static setRecord (recordTime) {
        const today = new Date()
        today.setTime(today.getTime() + (365 * 24 * 60 * 60 * 1000))

        document.cookie = 'record=' + recordTime + ';expires=' + today.toUTCString()  + ';path=/'
        console.log('record=' + recordTime + ';expires=' + today.toUTCString() + ';path=/')
        console.log(document.cookie)
    }
}
